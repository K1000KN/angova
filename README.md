# Angova 

Projet d'étude Epitech en équipe 🎓

**Equipe :**

- Kamil N
- Kamil K
- Laurent
- Mathis
- Samy
- Patrick

![bannière](./ressources/Angova.png)


## 1. <a name='Tabledesmatires'></a> Table des matières

*  1. [ Table des matières](#Tabledesmatires)
*  2. [Pourquoi ce dépôt ?](#Pourquoicedpt)
*  3. [La technologie](#Technos)
*  4. [Gestion de projet](#GestionDeProjet)
	* 4.1. [Les labels](#Leslabels)
	* 4.2. [Le board](#LeBoard)
	* 4.3. [Les branches](#Lesbranches)
	* 4.4. [Les commits](#Lescommits)
	* 4.5. [Modèles pour les issues et les merges requests](#Modlespourlesissuesetlesmergesrequests)
*  5. [Les fonctionnalités](#Lesfonctions)

## 2. <a name='Pourquoicedpt'></a>Pourquoi ce dépôt ?

Ce dépot à pour objectif de fournir un outil de démarrage rapide pour organiser et gérer un nouveau projet avec GitLab.

Partant du constat que le démarrage d'un projet est un processus long et complexe, j'ai voulu fournir un outil simple et efficace pour lancer un projet avec GitLab et ses outils !

## 3. <a name='Technos'></a>La technologie

- Front : React / NextJS
- Back (API) : NestJS / NodeJS
- Base de donnée : MongoDB
- Conteneurisation : Docker

## 4. <a name='gestion'></a>Gestion de projet

- Une branche par ticket
- Un ticket par personne

<!-- **[Sprint 1 - Mise en place du projet](https://gitlab.com/)**  MILESTONES DU PROJET -->


### 4.1. <a name='Leslabels'></a>Les labels

Les labels sont des éléments qui sont associés à des issues et merges requests et qui permettent de les classer, le organiser et de les identifier simplement.

![label](.ressources/label.gif)

### 4.2. <a name='LeBoard'></a>Le board  

Le Board est l'outil central de GitLab pour organiser et gérer votre projet.

Il va vous permettre de visualiser les différentes tâches que vous avez à accomplir, et de suivre leur progression.

La structure de ce board adopte l'approche [Scrumban](https://asana.com/fr/resources/scrumban).

![board](.ressources/board.gif)


### 4.3. <a name='Lesbranches'></a>Les branches

Les trois branches proposées dans ce template permettent de gérer de manière simple l'état du déploiement de votre projet.
 
Elles sont particulièrement utiles si vous leurs associez des pipelines GitLab CI pour automatiser le déploiement de votre projet en fonction des branches.

Ce modèle s'inspire librement de l'approche [GitLab Flow](https://www.youtube.com/watch?v=ZJuUz5jWb44).

- Master : (branche utilisée pour les README etc)
- Pré-production
- Production

**Branche d'exemple** : feature/front#001-mise-en-place

**IMPORTANT :**  Avant de créer une branche, passez sur la branche develop. 
Faites de préférence toujours un git pull pour être sûr d’être à jour. La nouvelle branche que vous venez de créer a été rapatriée. 
Après cela, fâites un git checkout -b [feature/front#001-mise-en-place]
  
### 4.4. <a name='Lescommits'></a>Les commits

**Exemple de message de commit** : front#001 Mise en place

**Rappel :** `git commit -m "front#001 Mise en place"`
  
### 4.5. <a name='Modlespourlesissuesetlesmergesrequests'></a>Modèles pour les issues et les merges requests

Ce template propose des modèles pour les issues et les merges requests afin de simplifier et standardiser leur utilisation par les équipes du projet.

![board](.ressources/issue.gif)

![board](.ressources/mr.gif)

## 5. <a name='Lesfonctions'></a>Les fonctionnalités
  
Les fonctionnalités du projet se trouvent dans le cahier des charges (Canva)
